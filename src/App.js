import React from 'react';
import './App.css';

function getTitle(title) {
  return title;
}
const welcome = {
  greeting: 'Hey',
  title: 'React'
};

const Rlist = [
  {
    title: 'React',
    url: 'https://reactjs.org/',
    author: 'Jordan Walke',
    num_comments: 3,
    points: 4,
    objectID: 0,
  },
  {
    title: 'Redux',
    url: 'https://redux.js.org/',
    author: 'Dan Abramov, Andrew Clark',
    num_comments: 2,
    points: 5,
    objectID: 1,
  },
];  


const Blist = [
  {
    title: 'BlairWitch',
    url: 'https://reactjs.org/',
    author: 'Not Sure',
    num_comments: 3,
    points: 4,
    objectID: 0,
  },
  {
    title: 'BombsAway',
    url: 'https://redux.js.org/',
    author: 'Mario',
    num_comments: 2,
    points: 5,
    objectID: 1,
  },
];

const List = (props) => {
  return props.list.map(function (item) {
    return (
      <div key={item.objectID}>
        <span class="pad"><a href={item.url}>{item.title}</a></span>
        <span class="pad">{item.author}</span>
        <span class="pad">{item.num_comments}</span>
        <span class="pad">{item.points}</span>

        {item.title}
      </div>);
  })
}
function Search() {
  const handleChange = event =>{
    console.log(event);
  }
  return (<div>
    <label htmlFor="search">Search: </label>
    <input draggable id="search" type="text" placeholder='curious?' onChange={handleChange} />
  </div>
  )
}

function App() {
  
  return (

    <div>
      <h1 draggable>My Hacker Stories</h1>
      <h2>{welcome.greeting} {welcome.title}</h2>
      <Search />
      <Search />
      <hr />
      <List list={Rlist} />
      <List list={Blist}/>
      <Search />
    </div>
  );
}

export default App;
